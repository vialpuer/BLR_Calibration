# -*- coding: utf-8 -*-
"""
Created on Tue Jul 05 15:18:42 2016

@author: viherbos
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as pl

import os


def panel_to_hdf5(in_path, n_PMT, n_events, n_samples, out_path):

	# Reads signals from each event and PMT stored in text files
	# Creates HDF5 based on a single PANEL node where Item=PMT /
	# minor_axis(column)=Event / major_axis=sample

	if os.path.exists(out_path):
		os.remove(out_path)

	store = pd.HDFStore(out_path, complevel=9, complib='zlib')


	data = pd.Panel(dtype='int32',
			     items=range(0,n_PMT),
			     minor_axis=range(0,n_events),
                      major_axis=range(0,n_samples))
	# items: axis 0, each item corresponds to a DataFrame contained inside
	# major_axis: axis 1, it is the index (rows) of each of the DataFrames
	# minor_axis: axis 2, it is the columns of each of the DataFrames


	#The following reads all the data and stores it in a Panel
	for x in range(1, n_PMT+1):
		# Read all the files in the dataset
		for y in range(1, n_events+1):
			num = (x-1)*n_events+y
			path=''.join([in_path,str(num),'.txt'])
			print path
			# File to read
			g=pd.read_csv(path, names=[str(y-1)], header=None, dtype='int32')
			# DataFrame that stores read data and gives columns names.
			# Each column is an event
			data.loc[x-1,:,y-1]=g[str(y-1)]
			#Store data colum in its place

	store.put('data',data)
	# Dumps data to file
	store.close()


def read_panel_hdf5(in_path, PMT, event):

	#Reads PMT signal realted to a given event from HDF5 file

	a = pd.read_hdf(in_path,'data')

	b = np.array(a[PMT,:,event])

	return b


def get_panel_hdf5(in_path):

	#Reads PMT signal related to a given event from HDF5 file

	a = pd.read_hdf(in_path,'data')

	return a


def cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path):

	# Reads signals from calibration file stored in text files
	# Creates HDF5 based on a single PANEL node where Item=CHANNEL /
	# minor_axis(column)=Event / major_axis=sample

	if os.path.exists(out_path):
		os.remove(out_path)

	store = pd.HDFStore(out_path, complevel=9, complib='zlib')


	data = pd.Panel(dtype='int32',
			     items=range(0,n_CHANNELS),
			     minor_axis=range(0,n_events),
                 major_axis=range(0,n_samples))
	# items: axis 0, each item corresponds to a DataFrame contained inside
	# major_axis: axis 1, it is the index (rows) of each of the DataFrames
	# minor_axis: axis 2, it is the columns of each of the DataFrames

	index_dataF = pd.read_csv(index_path, names=[0], header=None, dtype='int32')

	index = index_dataF.values

	print index

	#The following reads all the data and stores it in a Panel
	for x in range(0, n_CHANNELS):
		# Read all the files in the dataset
		for y in range(1, n_events+1):
			#num = (x-1)*n_events+y

			# if (index.item(x) == 0): 
			# 	even_odd = 0
			# else:
			# 	even_odd = x-(x/2)*2
			even_odd = 5

			path=''.join([in_path,str(index.item(x)),'.root/pmt_',str(even_odd),'_trace_evt_',str(y),'.txt'])
			print path
			# File to read
			g=pd.read_csv(path, names=[0], header=None, dtype='double')
			# DataFrame that stores read data and gives columns names.
			# Each column is an event
			data.loc[x,:,y-1]=g[0]
			#Store data colum in its place

	store.put('data',data)
	# Dumps data to file
	store.close()



def main():

	#SPE
	# in_path  = 'F:/DATOS_DAC/spe_1230/2046/pmt_0_trace_evt_'
	# out_path = 'spe_1230_2046.h5.z'
	# n_PMT   = 1
	# n_events = 5000
	# n_samples = 3200

	# in_path  = 'D:/DATOS_DAC/2491/pmt_0_trace_evt_'
	# out_path = 'spe.h5.z'
	# n_PMT   = 1
	# n_events = 2000
	# n_samples = 3200

	# in_path  = 'F:/DATOS_DAC/CALIBRATION/2306.root/pmt_0_trace_evt_'
	# out_path = 'F:/DATOS_DAC/CALIBRATION/cal_2306.h5.z'
	# n_PMT   = 1
	# n_events = 500
	# n_samples = 8000

	#Linearity
	# in_path  = 'D:/DATOS_DAC/2271/pmt_1_trace_evt_'
	# out_path = '2271.h5.z'
	# n_PMT   = 3
	# n_events = 50
	# n_samples = 6400

#	in_path  = 'D:/DATOS_DAC/Canfranc/Calibracion/run_1755'
#	out_path = 'Calibracion_1.h5.z'
#	n_PMT   = 12
#	n_events = 3
#	n_samples = 128000


	# panel_to_hdf5(in_path, n_PMT, n_events, n_samples, out_path)

	in_path  = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/'
	index_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/index_100u.txt'
	out_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/cal_100u.h5.z'
	n_CHANNELS   = 1
	n_events = 500
	n_samples = 16000

	cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path)


	in_path  = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/'
	index_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/index_50u.txt'
	out_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/cal_50u.h5.z'
	n_CHANNELS   = 1
	n_events = 500
	n_samples = 16000

	cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path)


	in_path  = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/'
	index_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/index_15u.txt'
	out_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/cal_15u.h5.z'
	n_CHANNELS   = 1
	n_events = 500
	n_samples = 8000

	cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path)

	
	in_path  = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/'
	index_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/index_5u.txt'
	out_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/cal_5u.h5.z'
	n_CHANNELS   = 1
	n_events = 500
	n_samples = 8000

	cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path)


	in_path  = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/'
	index_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/index_2u5.txt'
	out_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/cal_2u5.h5.z'
	n_CHANNELS   = 1
	n_events = 500
	n_samples = 8000

	cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path)


	in_path  = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/'
	index_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/index_1u.txt'
	out_path = '/mnt/WINDOWS_ntfs/DATOS_DAC/CALIBRATION_FEE_V4/cal_1u.h5.z'
	n_CHANNELS   = 1
	n_events = 500
	n_samples = 8000

	cal_to_hdf5(index_path, in_path, n_CHANNELS, n_events, 
				n_samples, out_path)


#	res = read_panel_hdf5(out_path, 0, 2)
#
#	pl.plot(res)
#	pl.show()

if __name__ == "__main__":
	main()