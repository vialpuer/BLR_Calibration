
import sys
import os


import numpy as np
import matplotlib.pyplot as plt
from scipy import signal


def find_baseline(x):
    # Finds baseline in a given sequence 
    length_signal = np.size(x)
    baseline = x.sum() / length_signal      
    return baseline 

def discharge(length_d=5000, tau=2500, compress=0.0025):
    t_discharge = np.arange(0,length_d,1,dtype=np.double)
    discharge_curve = compress*(1-1/(1+np.exp(-(t_discharge-length_d/2)/tau)))+(1-compress)
    return discharge_curve


def BLR_ACC3_i(signal_daq, coef, thr = 2, acum_FLOOR=1000, 
            coef_clean=1, filter=False, i_factor=1):

  
    signal_daq = find_baseline(signal_daq[0:1400]) - signal_daq

    

    # Cleaning Filter
    if (filter==True):
        #C1=3100E-9; #C1=2714E-9;
        # R1=1567;
        # f_sample = (1/25E-9)
        # freq_zero = 1/(R1*C1);
        # freq_zerod = freq_zero / (f_sample*np.pi)

        b_cf, a_cf = signal.butter(1, coef_clean, 'high', analog=False);
        signal_daq = signal.lfilter(b_cf,a_cf,signal_daq)
    

    if (i_factor > 1):

        signal_daq_i = np.zeros(len(signal_daq)*i_factor,dtype=float)
        signal_daq_i[::i_factor] = signal_daq
        # Interpolated by zeros

        b_up, a_up = signal.butter(2, 1/(float(i_factor)*0.95), 'low', analog=False);
        # Smoothing filtering
        signal_daq_i = float(i_factor)*signal.lfilter(b_up,a_up,signal_daq_i)

        signal_daq = signal_daq_i

        # OFFSET correction in case a slight change in offset takes place
        signal_daq = signal_daq - find_baseline(signal_daq[0:i_factor*1400]) 
    
    

    len_signal_daq = len(signal_daq)
    acum = np.zeros(len_signal_daq, dtype=np.double)
    signal_r = np.zeros(len_signal_daq, dtype=np.double)
    j_reg = np.zeros(len_signal_daq, dtype=np.double)
    
    
#----------

    discharge_length = 5000
    discharge_curve = discharge(length_d=discharge_length, tau=2500, compress=0.0025)
    j=0

    for k in range(0,len_signal_daq): 

            
        trigger_linep = thr
       
        
        # Signal is always being recovered
        signal_r[k] = signal_daq[k] + signal_daq[k]*(coef/2.0) + coef*acum[k-1]
        acum[k] = acum[k-1] + signal_daq[k];
        
        # condition: Either raw signal raises above trigger line or accumulator 
        # shows a value above
        # the residual level (These are the conditions for a true pulse signal)
        # Long flat pulses might have a AC signal level below the threshold 
        # in the middle of the 
        # pulse so we need the second condition
        
        if (signal_daq[k] < trigger_linep) and (acum[k-1] < acum_FLOOR):
           
          
        # In this case acum should be driven to zero (avoiding negative values)
        # A smooth discharge function is being introduced though its effect is purely cosmetic
            
            if (acum[k-1]>0):
                acum[k]=acum[k-1]*(1-5*coef)#*discharge_curve[j]
                #if j<discharge_length-1:
                    #j=j+1
                #else:
                    #j=discharge_length-1
            else:
                acum[k]=0
                j=0
                       
        #else:
            
            # Acumulator evolves as in the original basic algorithm
            #j=0

            
    j_reg[k]=j        
            

    if (i_factor > 1):
        signal_r = signal.decimate(signal_r,i_factor)


    return  signal_r,acum,signal_daq



def BLR_ACC3(signal_daq, coef, thr = 2, acum_FLOOR=1000, 
            coef_clean=1, filter=False):

  
    signal_daq = find_baseline(signal_daq[0:1400]) - signal_daq

    

    # Cleaning Filter
    if (filter==True):
        #C1=3100E-9; #C1=2714E-9;
        # R1=1567;
        # f_sample = (1/25E-9)
        # freq_zero = 1/(R1*C1);
        # freq_zerod = freq_zero / (f_sample*np.pi)

        b_cf, a_cf = signal.butter(1, coef_clean, 'high', analog=False);
        #entry = find_baseline(signal_daq[0:1400])
        #signal_daq_aux = np.concatenate((entry*np.ones(2000),signal_daq))
        signal_daq = signal.lfilter(b_cf,a_cf,signal_daq)
        #signal_daq = signal_daq[2000:]

        #signal_daq = signal_daq - find_baseline(signal_daq[0:1400])
    

    
    len_signal_daq = len(signal_daq)
    acum = np.zeros(len_signal_daq, dtype=np.double)
    signal_r = np.zeros(len_signal_daq, dtype=np.double)
    j_reg = np.zeros(len_signal_daq, dtype=np.double)
    
    
#----------

    discharge_length = 5000
    discharge_curve = discharge(length_d=discharge_length, tau=2500, compress=0.0025)
    j=0

    for k in range(0,len_signal_daq): 

            
        trigger_linep = thr
       
        
        # Signal is always being recovered
        signal_r[k] = signal_daq[k] + signal_daq[k]*(coef/2.0) + coef*acum[k-1]
        acum[k] = acum[k-1] + signal_daq[k];
        
        # condition: Either raw signal raises above trigger line or accumulator 
        # shows a value above
        # the residual level (These are the conditions for a true pulse signal)
        # Long flat pulses might have a AC signal level below the threshold 
        # in the middle of the 
        # pulse so we need the second condition
        
        if (signal_daq[k] < trigger_linep) and (acum[k-1] < acum_FLOOR):
           
          
        # In this case acum should be driven to zero (avoiding negative values)
        # A smooth discharge function is being introduced though its effect is purely cosmetic
            
            if (acum[k-1]>0):
                acum[k]=acum[k-1]*(1-5*coef)#*discharge_curve[j]
                #if j<discharge_length-1:
                    #j=j+1
                #else:
                    #j=discharge_length-1
            else:
                acum[k]=0
                j=0
                       
        #else:
            
            # Acumulator evolves as in the original basic algorithm
            #j=0

            
    j_reg[k]=j        
            



    return  signal_r,acum,signal_daq



