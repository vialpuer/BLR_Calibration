import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tables as tb
import multiprocessing as mp
from functools import partial
import os



def read_DATE_hdf5(in_path, SIPM, event):
    #Reads SIPM signal realted to a given event from HDF5 file
    a = pd.HDFStore(in_path)
    b = np.array(a.root.RD.sipmrwf)
    c = b[event,SIPM,:]
    return c