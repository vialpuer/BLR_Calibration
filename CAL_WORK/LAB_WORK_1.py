import sys
import numpy as np
import csv
import os as os
import pandas as pd
from PyQt5 import QtCore, QtWidgets, uic
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

# My functions
from MP_wrappers import BLR_lambda
from MP_wrappers import BLR_batch
from cal_IO_lib import read_CALHF
from cal_IO_lib import read_DATE_hdf5
from cal_IO_lib import DATE_to_CALHF_MP
from DBLR_lab import find_baseline
from fit_library import gauss
import fit_library as fit


qtCreatorFile = "LAB_WORK_1.ui" # Enter file here.

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class LAB_data():
    fig1 = Figure()
    fig2 = Figure()
    fig3 = Figure()
    fig4 = Figure()

 
    axes={'ax1':0,'ax2':0,'ax3':0,'ax4':0, 'ax5':0}
    # Axes 1 & 2 for figure 1. Axes 3 for figure 2
    baseline_txt = fig1.text(0.6,0.8, ('BASELINE = '))
    mu_txt = fig2.text(0.15,0.8, ('MU = '))
    sigma_txt = fig2.text(0.15,0.75, ('SIGMA = '))
    res_txt = fig2.text(0.15,0.7, ('RES(fwhm) = '))
    lin_txt = fig3.text(0.15,0.7, ('CHI2_r = '))
    lin2_txt = fig3.text(0,0.91, ('Photons'))
    lin3_txt = fig3.text(0.8,0.02, ('Pulse (us)'))
    mu_txt_spe = fig4.text(0.15,0.8, ('MUs = '))
   # mu_txt_spe2 = fig4.text(0,0.91, ('Hits'))
   # mu_txt_spe3 = fig4.text(0.8,0.02, ('Counts (25ns)'))
    sigma_txt_spe = fig4.text(0.15,0.75, ('SIGMAs = '))

    
    site = "/home/hwuser/PMTsArea/data/hdf5/data/"
    #site = "F:/"

    #def_path = site + "DATOS_DAC/CALIBRATION_FEB_2017_NB/"
    def_path ="/home/hwuser/PMTsArea/data/hdf5/data/"

    #files={'fname':site + "/home/hwuser/PMTsArea/data/hdf5/data/run_189_0000_waveforms.h5",
    #      'conf_name':"../COEFF.conf",
    #      'dir':site + "DATOS_DAC/CALIBRATION_FEB_2017_NB/"}
    files={'fname':"/home/hwuser/PMTsArea/data/hdf5/data/run_189_0000_waveforms.h5",
          'conf_name':"/home/hwuser/PMTsArea/LAB_work/COEFF.conf",
          'dir':"/home/hwuser/PMTsArea/data/hdf5/data/"}


    prefix={'a':"box0_"}


    PMT_data = {'PMT_n':2, 'event_n':2, 'blr':0.53E-3, 'cf':0.3E-6,
                  'thr':5, 'spe':20.0, 'accum_floor':3500, 'i':1, 'thre':15}

    flags = {'config':False,'DATE':False}

    hist = {'bins':50, 'Low_limit':10, 'High_limit':1000000, 'n_events':250}

    FECal_DATA = np.ones((24,3),dtype=float)
    FECal_DATA[:,1]=FECal_DATA[:,1]*PMT_data['blr']
    FECal_DATA[:,2]=FECal_DATA[:,2]*PMT_data['cf']

    energy_raw = []

    translate = {'input_p':"/home/hwuser/PMTsArea/data/hdf5/",
                 'output_p':"/home/hwuser/PMTsArea/data/",
                 'FEE_box':0,
                 'MP':2,
                 'pulse_list':"50,100,150"}

    integral=[]
    find_spe ={'path_name':"/home/hwuser/PMTsArea/data/hdf5/data/run_739_0000_waveforms.h5",
               'PMT_n':0,
               'period':1000,
               'start':10000,
               'spe_len':10,
               'bins':50,
               'events':100,
               'guess':15,
               'Low_limit':10,
               'High_limit':1000000}


class CORE():
    def __init__(self,upper_class):
        self.uc = upper_class

    def f_DBLR(self):

        try:
            if (self.uc.checkBox_DATE.isChecked() == True):
                f = read_DATE_hdf5(self.uc.d.files['fname'],
                                    int(self.uc.spinBox_pmt.value()),
                                    int(self.uc.spinBox_event.value()))
            else:
                f = read_CALHF(self.uc.d.files['fname'],
                                    int(self.uc.spinBox_pmt.value()),
                                    int(self.uc.spinBox_event.value()))
        except IndexError:
            print ("Index OUT of RANGE")
            return 0

        output_BLR = BLR_lambda( f,
                                 coef = self.uc.d.PMT_data['blr'],
                                 thr = self.uc.d.PMT_data['thr'],
                                 acum_FLOOR = self.uc.d.PMT_data['accum_floor'],
                                 coef_clean = self.uc.d.PMT_data['cf'],
                                 filter=self.uc.checkBox_filt.isChecked(),
                                 i_factor = self.uc.d.PMT_data['i'],
                                 e_thr = self.uc.d.PMT_data['thre'],
                                 SPE = self.uc.d.PMT_data['spe'] )

        print (output_BLR['ENERGY'])

        self.uc.lcdNumber_2.display(np.std(output_BLR['recons'][100:1000],ddof=1)) #NOISE
        self.uc.lcdNumber.display(output_BLR['ENERGY'])
        self.uc.start_t.setText(str(output_BLR['LIMIT_L']))
        self.uc.end_t.setText(str(output_BLR['LIMIT_H']))

        self.uc.d.axes['ax1']
        self.uc.d.axes['ax1'].plot(output_BLR['recons'])
       
        if (self.uc.checkBox_accu.isChecked() == True):
            self.uc.d.axes['ax2'].plot(output_BLR['acum'])
        if (self.uc.checkBox_pre.isChecked() == True):
            self.uc.d.axes['ax1'].plot(output_BLR['signal_daq'])

        self.uc.d.baseline_txt.remove()
        self.uc.d.baseline_txt=self.uc.d.fig1.text(0.6,0.8, ('BASELINE = %0.2f'
                                          % (find_baseline(f[:1400]))))

       
        self.uc.canvas1.draw()


    def f_RES(self):

        self.uc.d.energy_raw = BLR_batch(self.uc.d.files['fname'],
                                    coef = self.uc.d.PMT_data['blr'],
                                    thr = self.uc.d.PMT_data['thr'],
                                    acum_FLOOR = self.uc.d.PMT_data['accum_floor'],
                                    coef_clean = self.uc.d.PMT_data['cf'],
                                    SPE = self.uc.d.PMT_data['spe'],
                                    e_thr = self.uc.d.PMT_data['thre'],
                                    filter = self.uc.checkBox_filt.isChecked(),
                                    i_factor = self.uc.d.PMT_data['i'],
                                    point = self.uc.d.PMT_data['PMT_n'],
                                    n_events = self.uc.d.hist['n_events']
                                    )

        self.uc.redraw_b.clicked.emit()


    def f_LIN(self):

        # Reads hdf5 files in given directory and gets mean, sigma and pulse width
        # (pulse width from file name).Example: cal_2u5.h5.z -> prefix:cal_
        #                                                    -> 2.5 usec

        path = self.uc.d.files['dir']
        bins_ER = self.uc.d.hist['bins']

        files=[]
        files_hdf5=[]

        for (dirpath, dirnames, filenames) in os.walk(path):
            files.extend(filenames)
            break

        for i in range(len(files)):
            if (('.h5' in files[i]) and (self.uc.d.prefix['a'] in files[i])):
                files_hdf5.append(files[i])
        # Filters file names to find h5.z files

        print (files_hdf5)

        mu_s=[]
        error_mu_s=[]
        sigma_s=[]
        error_sigma_s=[]
        X_point=[]
        LIMITL_hist = float(self.uc.Low_limit_t.text())
        LIMITH_hist = float(self.uc.High_limit_t.text())

        for pulse in files_hdf5:

            pulse_lit = pulse[len(self.uc.d.prefix['a']):len(pulse)-len('.h5')]

            pulse_lit = pulse_lit.replace('u','.')

            X_point.append(float(pulse_lit))

            energy_raw =  BLR_batch(path+pulse,
                                coef = self.uc.d.PMT_data['blr'],
                                thr = self.uc.d.PMT_data['thr'],
                                acum_FLOOR = self.uc.d.PMT_data['accum_floor'],
                                coef_clean = self.uc.d.PMT_data['cf'],
                                SPE = self.uc.d.PMT_data['spe'],
                                e_thr = self.uc.d.PMT_data['thre'],
                                filter = self.uc.checkBox_filt.isChecked(),
                                i_factor = self.uc.d.PMT_data['i'],
                                point = self.uc.d.PMT_data['PMT_n'],
                                n_events = self.uc.d.hist['n_events']
                                )
            print (path+pulse)
            mean = np.mean(energy_raw)
            LIMITL_hist = 0.75*mean
            LIMITH_hist = 1.25*mean

            condition = (energy_raw > LIMITL_hist)*(energy_raw < LIMITH_hist)
            energy = np.extract(condition,energy_raw)
            print (energy)

            #Outlayers Filtering
            try:
                coeff_fit, perr, hist, bin_centres = fit.gauss1_fit(energy,
                                                                '','','',
                                                                self.uc.d.hist['bins'],1,0)
                hist_fit = fit.gauss(bin_centres, coeff_fit[0],coeff_fit[1],coeff_fit[2])
                res = np.abs(coeff_fit[2])*2.35/coeff_fit[1]*100.0
                err_res =  2.35*100.0*(np.abs(perr[2]/coeff_fit[1])+np.abs(perr[1]*coeff_fit[2])/np.square(coeff_fit[1]))

            except RuntimeError:
                print ("Fitting Problems")
                hist_fit = 0
                bin_centres = 0
                err_res = 0
                coeff_fit = np.zeros(4)
                perr = np.zeros(4)

            mu_s.append(coeff_fit[1])
            error_mu_s.append(perr[1])
            sigma_s.append(coeff_fit[2])
            error_sigma_s.append(perr[2])


        # mu_s = [10,20,30,40]
        # sigma_s = [1,1,2,4]
        # X_point = [200,100,400,300]


        mu_s = np.array(mu_s)
        sigma_s = np.array(sigma_s)
        X_point = np.array(X_point)

        # Sort the point array (the order was established by os.walk)
        index_sort   = X_point.argsort()
        X_point      = X_point[index_sort]
        X_point      = X_point[np.newaxis]
        sigma_s      = sigma_s[index_sort]
        sigma_s      = sigma_s[np.newaxis]
        mu_s         = mu_s[index_sort]
        mu_s         = mu_s[np.newaxis]

        # Write File with fitting data (sigma and mu for every point)

        d_array = np.concatenate((  X_point,
                                    mu_s,
                                    sigma_s),axis=0)
        print (d_array.T)
        df_linearity=pd.DataFrame(data=d_array.T,dtype=float,columns=['P_LENGTH','MU','SIGMA'])
        file_csv = self.uc.d.prefix['a']+'ch_'+str(self.uc.d.PMT_data['PMT_n'])
        df_linearity.to_csv('Linearity_'+ file_csv +'.csv' ,float_format='%.4e')

        # Line Fit
        mu_s=mu_s[0,:]; X_point=X_point[0,:]; sigma_s=sigma_s[0,:]

        coeff_lin, perr_lin, XI2_r = fit.line_fit(mu_s,X_point,sigma_s,'','','',1,0)

        print (coeff_lin, XI2_r)

        #Plots fit results for a point b point test
        self.uc.d.axes['ax4'].errorbar(X_point,mu_s,fmt='r.',yerr=sigma_s)
        self.uc.d.axes['ax4'].plot(X_point,fit.line(X_point,coeff_lin[0],coeff_lin[1]))
        #
        axes = self.uc.d.fig2.gca()
        axes.set_xlim([0.9*X_point[0],X_point[len(X_point)-1]*1.1])


        self.uc.d.fig3.suptitle("Energy Measurement Linearity")
        self.uc.d.lin_txt.remove()
        self.uc.d.lin_txt=self.uc.d.fig3.text(0.15,0.8, ('CHI_2_R = %0.3f' % (XI2_r)))
        self.uc.d.lin2_txt=self.uc.d.fig3.text(0,0.91, ('Photons'))
        self.uc.d.lin3_txt=self.uc.d.fig3.text(0.8,0.02, ('Pulse (us)'))
        self.uc.canvas3.draw()


    def f_translate(self):

        karg={}
        karg['box_number'] = self.uc.d.translate['FEE_box']
        karg['output_path'] = self.uc.d.translate['output_p']
        karg['input_path'] = self.uc.d.translate['input_p']
        karg['MP'] = self.uc.d.translate['MP']
        karg['pulse_length']=np.fromstring(self.uc.d.translate['pulse_list'],
                                           dtype=int,sep=',')

        #print np.fromstring(self.uc.d.translate['pulse_list'],dtype=int,sep=',')
        print ("Entrando")
        DATE_to_CALHF_MP(**karg)


    def f_findSPE(self):
        event=0
        pulse_start = self.uc.d.find_spe['start']
        pulse_period = self.uc.d.find_spe['period']
        spe_length = self.uc.d.find_spe['spe_len']

        self.uc.d.integral=[]

        for event in range(0,self.uc.d.find_spe['events']):
            f = read_DATE_hdf5( self.uc.d.find_spe['path_name'],
                                self.uc.d.find_spe['PMT_n'],
                                event)
            bl = find_baseline(f[:pulse_start-100])
            # Stable Baseline
            f = bl - f
            length = len(f)
            # Take integral of all the pulses in event
            event_length = len(f)
            for pulse in range(pulse_start,event_length,pulse_period):
                self.uc.d.integral = np.append(self.uc.d.integral,
                                               np.sum(f[pulse:pulse+spe_length]))

        self.uc.redraw_b_2.clicked.emit()



class Aux_Buttons():
    def __init__(self,upper_class):
        self.uc = upper_class

    def f_quit(self):
        QtCore.QCoreApplication.instance().quit()

    def f_clear(self,option):

        if (option==1):
            self.uc.d.axes['ax1'].cla()
            self.uc.d.axes['ax2'].cla()
            for txt in self.uc.d.fig1.texts:
                txt.set_visible(False)
            self.uc.canvas1.draw()
        elif (option==2):
            self.uc.d.axes['ax3'].cla()
            for txt in self.uc.d.fig2.texts:
                txt.set_visible(False)
            self.uc.canvas2.draw()
        elif (option==3):
            self.uc.d.axes['ax4'].cla()
            for txt in self.uc.d.fig3.texts:
                txt.set_visible(False)
            self.uc.canvas3.draw()
        elif (option==4):
            self.uc.d.axes['ax5'].cla()
            for txt in self.uc.d.fig4.texts:
                txt.set_visible(False)
            self.uc.canvas4.draw()


    def f_CONFIG(self):
        data_file = open(self.uc.d.files['conf_name'])
        data_reader = csv.reader(data_file)

        FECal_DATA_c = np.array(list(data_reader))
        self.uc.d.FECal_DATA = FECal_DATA_c[1:,:].astype(np.float)

        self.uc.blr_t.setText(str(self.uc.d.FECal_DATA[self.uc.spinBox_pmt.value(),1]))
        self.uc.cf_t.setText(str(self.uc.d.FECal_DATA[self.uc.spinBox_pmt.value(),2]))

        print (self.uc.d.FECal_DATA)
        self.uc.d.flags['config'] = True

    def f_coeff_update(self):
        if (self.uc.d.flags['config'] == True):
            self.uc.blr_t.setText(str(self.uc.d.FECal_DATA[self.uc.spinBox_pmt.value(),1]))
            self.uc.cf_t.setText(str(self.uc.d.FECal_DATA[self.uc.spinBox_pmt.value(),2]))
            self.uc.blr_t_2.setText(str(self.uc.d.FECal_DATA[self.uc.spinBox_pmt_2.value(),1]))
            self.uc.cf_t_2.setText(str(self.uc.d.FECal_DATA[self.uc.spinBox_pmt_2.value(),2]))

    def f_redraw(self):

        #Outlayers Filtering
        LIMITL_hist = float(self.uc.Low_limit_t.text())
        LIMITH_hist = float(self.uc.High_limit_t.text())

        condition = (self.uc.d.energy_raw>LIMITL_hist)*(self.uc.d.energy_raw<LIMITH_hist)
        energy = np.extract(condition,self.uc.d.energy_raw)

        print (energy)

        try:
            coeff_fit, perr, hist, bin_centres = fit.gauss1_fit(energy,
                                                            '','','',
                                                            self.uc.d.hist['bins'],1,0)
            hist_fit = fit.gauss(bin_centres, coeff_fit[0],coeff_fit[1],coeff_fit[2])
            res = np.abs(coeff_fit[2])*2.35/coeff_fit[1]*100.0
            err_res =  2.35*100.0*(np.abs(perr[2]/coeff_fit[1])+np.abs(perr[1]*coeff_fit[2])/np.square(coeff_fit[1]))
        except RuntimeError:
            print ("Fitting Problems")
            hist_fit = 0
            bin_centres = 0
            err_res = 0
            res = 0
            coeff_fit = np.zeros(3)
            perr = np.zeros(3)

        self.uc.d.axes['ax3'].hist(energy, self.uc.d.hist['bins'], facecolor='green')
        self.uc.d.axes['ax3'].plot(bin_centres, hist_fit, 'r--', linewidth=1)
        self.uc.d.axes['ax3'].grid(True)
        #pinta2_ER.xlabel("Energy (pe)")
        #pinta_ER.ylabel("Hits")

        self.uc.d.fig2.suptitle("Energy Resolution")

        self.uc.d.mu_txt.remove()
        self.uc.d.sigma_txt.remove()
        self.uc.d.res_txt.remove()
        self.uc.d.mu_txt=self.uc.d.fig2.text(0.15,0.8, ('MU = %0.3f ( +/- %0.2f)' % (coeff_fit[1] , perr[1])))
        self.uc.d.sigma_txt=self.uc.d.fig2.text(0.15,0.75, ('SIGMA = %0.3f ( +/- %0.2f)' % (np.abs(coeff_fit[2]) , perr[2])))
        self.uc.d.res_txt=self.uc.d.fig2.text(0.15,0.70, ('RES(fwhm) = %0.2f (+/- %0.2f )(percent)' % (res, err_res)))

        self.uc.canvas2.draw()


    def f_spe_redraw_b(self):

        #Outlayers Filtering
        LIMITL_hist = float(self.uc.Low_limit_t_3.text())
        LIMITH_hist = float(self.uc.High_limit_t_3.text())

        condition = (self.uc.d.integral>LIMITL_hist)*(self.uc.d.integral<LIMITH_hist)
        energy = np.extract(condition,self.uc.d.integral)


        try:
            coeff_fit, perr, hist, bin_centres = fit.gauss3_fit(energy,
                                                            '','','',
                                                            self.uc.d.find_spe['bins'],
                                                            [self.uc.d.find_spe['guess'],
                                                            2.0*self.uc.d.find_spe['guess'],
                                                            3.0*self.uc.d.find_spe['guess']],
                                                            1,0)
            hist_fit = fit.gauss3(  bin_centres,
                                    coeff_fit[0],coeff_fit[1],coeff_fit[2],
                                    coeff_fit[3],coeff_fit[4],coeff_fit[5],
                                    coeff_fit[6],coeff_fit[7],coeff_fit[8],
                                    )

        except RuntimeError:
            print ("Fitting Problems")
            hist_fit = 0
            bin_centres = 0
            coeff_fit = np.zeros(10)
            perr = np.zeros(10)

        self.uc.d.axes['ax5'].hist(energy, self.uc.d.find_spe['bins'], facecolor='green')
        self.uc.d.axes['ax5'].plot(bin_centres, hist_fit, 'r--', linewidth=1)
        self.uc.d.axes['ax3'].grid(True)


        self.uc.d.fig4.suptitle("Energy Resolution")

        self.uc.d.mu_txt_spe.remove()
        self.uc.d.sigma_txt_spe.remove()
        self.uc.d.mu_txt_spe=self.uc.d.fig4.text(0.15,0.8,
            ('MU0=%0.1f (+/-%0.1f) MU1=%0.1f (+/-%0.1f) MU2=%0.1f (+/- %0.1f)' % (coeff_fit[1], perr[1],
                                                                                  coeff_fit[4], perr[4],
                                                                                  coeff_fit[7], perr[7] )))
        #self.uc.d.mu_txt_spe2=self.uc.d.fig4.text(0,0.91,('Hits'))
        #self.uc.d.mu_txt_spe3=self.uc.d.fig4.text(0.8,0.02,('Counts (25ns)'))
        self.uc.d.sigma_txt_spe=self.uc.d.fig4.text(0.15,0.75,
            ('SIGMA0 = %0.1f ( +/- %0.1f) SIGMA1 = %0.1f ( +/- %0.1f) SIGMA2 = %0.1f ( +/- %0.1f) ' % (np.abs(coeff_fit[2]), perr[2],
                                                                                                       np.abs(coeff_fit[2]), perr[2],
                                                                                                       np.abs(coeff_fit[2]), perr[2])))
        self.uc.canvas4.draw()


    # Controlled casting to avoid data intro errors
    def float_v(self,number):
        try:
            return float(number)
        except ValueError:
            return 0.0

    def int_v(self,number):
        try:
            return int(number)
        except ValueError:
            return 0

    def store_data(self):
        self.uc.d.PMT_data['PMT_n'] = self.int_v(self.uc.spinBox_pmt.value())
        self.uc.d.PMT_data['blr'] = self.float_v(self.uc.blr_t.text())
        self.uc.d.PMT_data['cf'] = self.float_v(self.uc.cf_t.text())
        self.uc.d.PMT_data['thr'] = self.float_v(self.uc.thr_t.text())
        self.uc.d.PMT_data['accum_floor'] = self.float_v(self.uc.accum_t.text())
        self.uc.d.PMT_data['i'] = self.int_v(self.uc.spinBox_i.value())
        self.uc.d.PMT_data['thre'] = self.float_v(self.uc.spinBox_thre.value())
        self.uc.d.PMT_data['spe'] = self.float_v(self.uc.spe_t.text())
        self.uc.d.hist['bins'] = self.int_v(self.uc.bins_t.text())
        self.uc.d.hist['n_events'] = self.int_v(self.uc.events_t.text())
        self.uc.d.hist['Low_limit'] = self.int_v(self.uc.Low_limit_t.text())
        self.uc.d.hist['High_limit'] = self.int_v(self.uc.High_limit_t.text())
        self.uc.d.prefix['a'] = self.uc.prefix_t.text()
        self.uc.d.files['dir'] = self.uc.file_path_t_3.text()
        self.uc.d.files['fname'] = self.uc.file_path_t.text()
        self.uc.d.translate['FEE_box'] = self.int_v(self.uc.spinBox_pmt_4.value())
        self.uc.d.translate['MP'] = self.int_v(self.uc.spinBox_pmt_5.value())
        self.uc.d.translate['pulse_list'] = self.uc.file_path_t_8.text()

        self.uc.d.find_spe['PMT_n'] = self.int_v(self.uc.spinBox_pmt_6.value())
        self.uc.d.find_spe['path_name'] = self.uc.file_path_t_6.text()
        self.uc.d.find_spe['period'] = self.int_v(self.uc.spe_period.text())
        self.uc.d.find_spe['events'] = self.int_v(self.uc.events_t_4.text())
        self.uc.d.find_spe['start'] = self.int_v(self.uc.spe_t_start.text())
        self.uc.d.find_spe['period'] = self.int_v(self.uc.spe_period.text())
        self.uc.d.find_spe['bins'] = self.int_v(self.uc.bins_t_3.text())
        self.uc.d.find_spe['guess'] = self.float_v(self.uc.spe_t_guess.text())
        self.uc.d.find_spe['spe_len'] = self.int_v(self.uc.spinBox_spe_range.value())
        self.uc.d.find_spe['events'] = self.int_v(self.uc.events_t_4.text())
        self.uc.d.find_spe['Low_limit'] = self.int_v(self.uc.Low_limit_t_3.text())
        self.uc.d.find_spe['High_limit'] = self.int_v(self.uc.High_limit_t_3.text())


    def mirror_DBLR2RES(self):
        # DBLR tab is the reference. Everything is stored there and copied to memory
        self.uc.spinBox_pmt_2.setValue(self.uc.spinBox_pmt.value())
        self.uc.blr_t_2.setText(self.uc.blr_t.text())
        self.uc.cf_t_2.setText(self.uc.cf_t.text())
        self.uc.thr_t_2.setText(self.uc.thr_t.text())
        self.uc.accum_t_2.setText(self.uc.accum_t.text())
        self.uc.spinBox_i_2.setValue(self.uc.spinBox_i.value())
        self.uc.spinBox_thre_2.setValue(self.uc.spinBox_thre.value())
        self.uc.spe_t_2.setText(self.uc.spe_t.text())
        self.uc.spinBox_pmt_3.setValue(self.uc.spinBox_pmt.value())
        self.uc.blr_t_3.setText(self.uc.blr_t.text())
        self.uc.cf_t_3.setText(self.uc.cf_t.text())
        self.uc.thr_t_3.setText(self.uc.thr_t.text())
        self.uc.accum_t_3.setText(self.uc.accum_t.text())
        self.uc.spinBox_i_3.setValue(self.uc.spinBox_i.value())
        self.uc.spinBox_thre_3.setValue(self.uc.spinBox_thre.value())
        self.uc.spe_t_3.setText(self.uc.spe_t.text())
        self.store_data()

    def mirror_RES2DBLR(self):
        self.uc.spinBox_pmt.setValue(self.uc.spinBox_pmt_2.value())
        self.uc.spinBox_thre.setValue(self.uc.spinBox_thre_2.value())
        self.uc.spe_t.setText(self.uc.spe_t_2.text())
        self.uc.bins_t_2.setText(self.uc.bins_t.text())
        self.uc.Low_limit_t_2.setText(self.uc.Low_limit_t.text())
        self.uc.High_limit_t_2.setText(self.uc.High_limit_t.text())
        self.uc.events_t_2.setText(self.uc.events_t.text())
        self.uc.file_path_t.setText(self.uc.file_path_t_2.text())
        self.store_data()

    def mirror_LIN2DBLR(self):
        self.uc.spinBox_pmt.setValue(self.uc.spinBox_pmt_3.value())
        self.uc.spinBox_thre.setValue(self.uc.spinBox_thre_3.value())
        self.uc.spe_t.setText(self.uc.spe_t_3.text())
        self.uc.bins_t.setText(self.uc.bins_t_2.text())
        self.uc.Low_limit_t.setText(self.uc.Low_limit_t_2.text())
        self.uc.High_limit_t.setText(self.uc.High_limit_t_2.text())
        self.uc.events_t.setText(self.uc.events_t_2.text())
        self.store_data()


class Browsers():
    def __init__(self,upper_class):
        self.uc = upper_class

    def path_browser(self):
        file_aux = QtWidgets.QFileDialog.getOpenFileName(self.uc,
                                        'Open file',
                                        self.uc.d.def_path,
                                        "Calibration Files H5 (*.h5)")

        fname_aux = ([str(x) for x in file_aux])
        self.uc.d.files['fname'] = fname_aux[0]
        #Trick for Qstring converting to standard string
        self.uc.file_path_t.setText(self.uc.d.files['fname'])
        self.uc.file_path_t_2.setText(self.uc.d.files['fname'])


    def directory_browser(self):
        file_aux = QtWidgets.QFileDialog.getExistingDirectory(self.uc,
                                        'Open Directory',
                                        self.uc.d.def_path)

        print (file_aux)
        self.uc.d.files['dir'] = file_aux+'/'
        #Trick for Qstring converting to standard string
        self.uc.file_path_t_3.setText(self.uc.d.files['dir'])


    def conf_path_browser(self):
        file_aux = QtWidgets.QFileDialog.getOpenFileName(self.uc,
                                        'Open file',
                                        './',
                                        "Configuration File (*.conf)")

        fname_aux = ([str(x) for x in file_aux])
        self.uc.d.files['conf_name'] = fname_aux[0]
        #Trick for Qstring converting to standard string
        self.uc.conf_file_t.setText(self.uc.d.files['conf_name'])


    def trans_browser_i(self):
        file_aux = QtWidgets.QFileDialog.getExistingDirectory(self.uc,
                                        'Open Directory',
                                        self.uc.d.translate['input_p'])

        print (file_aux)
        self.uc.d.translate['input_p'] = file_aux+'/'
        #Trick for Qstring converting to standard string
        self.uc.file_path_t_4.setText(self.uc.d.translate['input_p'])


    def trans_browser_o(self):
        file_aux = QtWidgets.QFileDialog.getExistingDirectory(self.uc,
                                        'Open Directory',
                                        self.uc.d.translate['output_p'])

        print (file_aux)
        self.uc.d.translate['output_p'] = file_aux+'/'
        #Trick for Qstring converting to standard string
        self.uc.file_path_t_5.setText(self.uc.d.translate['output_p'])


    def spe_browser(self):
        file_aux = QtWidgets.QFileDialog.getOpenFileName(self.uc,
                                        'Open file',
                                        self.uc.d.find_spe['path_name'],
                                        "Calibration Files H5 (*.h5)")
        fname_aux = ([str(x) for x in file_aux])
        self.uc.d.find_spe['path_name'] = fname_aux[0]
        self.uc.file_path_t_6.setText(self.uc.d.find_spe['path_name'])


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        #Data class
        self.d = LAB_data()
        self.c = CORE(self)
        self.browser_tools = Browsers(self)
        self.b_buttons = Aux_Buttons(self)
        # Passes all the needed information to the constructor of the aux class

        #Defaults
        #DBLR
        self.checkBox_filt.setChecked(True)
        self.checkBox_accu.setChecked(False)
        self.checkBox_pre.setChecked(False)
        self.conf_file_t.setText(self.d.files['conf_name'])
        self.file_path_t.setText(self.d.files['fname'])
        self.blr_t.setText(str(self.d.PMT_data['blr']))
        self.cf_t.setText(str(self.d.PMT_data['cf']))
        self.thr_t.setText(str(self.d.PMT_data['thr']))
        self.spe_t.setText(str(self.d.PMT_data['spe']))
        self.accum_t.setText(str(self.d.PMT_data['accum_floor']))
        self.spinBox_thre.setValue(self.d.PMT_data['thre'])
        #Resolution
        self.file_path_t_2.setText(self.d.files['fname'])
        self.blr_t_2.setText(str(self.d.PMT_data['blr']))
        self.cf_t_2.setText(str(self.d.PMT_data['cf']))
        self.thr_t_2.setText(str(self.d.PMT_data['thr']))
        self.accum_t_2.setText(str(self.d.PMT_data['accum_floor']))
        self.spe_t_2.setText(str(self.d.PMT_data['spe']))
        self.bins_t.setText(str(self.d.hist['bins']))
        self.Low_limit_t.setText(str(self.d.hist['Low_limit']))
        self.High_limit_t.setText(str(self.d.hist['High_limit']))
        self.events_t.setText(str(self.d.hist['n_events']))
        self.spinBox_thre_2.setValue(self.d.PMT_data['thre'])
        #Linearity
        #self.file_path_t_2.setText(self.d.files['fname'])
        self.blr_t_3.setText(str(self.d.PMT_data['blr']))
        self.cf_t_3.setText(str(self.d.PMT_data['cf']))
        self.thr_t_3.setText(str(self.d.PMT_data['thr']))
        self.accum_t_3.setText(str(self.d.PMT_data['accum_floor']))
        self.spe_t_3.setText(str(self.d.PMT_data['spe']))
        self.bins_t_2.setText(str(self.d.hist['bins']))
        self.Low_limit_t_2.setText(str(self.d.hist['Low_limit']))
        self.High_limit_t_2.setText(str(self.d.hist['High_limit']))
        self.spinBox_thre_3.setValue(self.d.PMT_data['thre'])
        self.file_path_t_3.setText(str(self.d.files['dir']))
        self.events_t_2.setText(str(self.d.hist['n_events']))
        self.prefix_t.setText(str(self.d.prefix['a']))
        #translate
        self.file_path_t_4.setText(str(self.d.translate['input_p']))
        self.file_path_t_5.setText(str(self.d.translate['output_p']))
        self.file_path_t_8.setText(str(self.d.translate['pulse_list']))
        self.spinBox_pmt_5.setValue(self.d.translate['MP'])
        #SPE
        self.file_path_t_6.setText(self.d.find_spe['path_name'])
        self.spe_period.setText(str(self.d.find_spe['period']))
        self.spe_t_start.setText(str(self.d.find_spe['start']))
        self.spe_t_guess.setText(str(self.d.find_spe['guess']))
        self.bins_t_3.setText(str(self.d.find_spe['bins']))
        self.events_t_4.setText(str(self.d.find_spe['events']))
        self.spinBox_spe_range.setValue(self.d.find_spe['spe_len'])
        self.Low_limit_t_3.setText(str(self.d.find_spe['Low_limit']))
        self.High_limit_t_3.setText(str(self.d.find_spe['High_limit']))


        #Button Calls
        self.quit_b.clicked.connect(self.b_buttons.f_quit)
        self.quit_b_2.clicked.connect(self.b_buttons.f_quit)
        self.go_DBLR_b.clicked.connect(self.c.f_DBLR)
        self.go_RES.clicked.connect(self.c.f_RES)
        self.clear_b.clicked.connect(lambda: self.b_buttons.f_clear(1))
        self.clear_b_2.clicked.connect(lambda: self.b_buttons.f_clear(2))

        self.file_path_b.clicked.connect(self.browser_tools.path_browser)
        self.file_path_b_2.clicked.connect(self.browser_tools.path_browser)
        self.conf_path_b.clicked.connect(self.browser_tools.conf_path_browser)
        self.load_config_b.clicked.connect(self.b_buttons.f_CONFIG)
        self.redraw_b.clicked.connect(self.b_buttons.f_redraw)

        self.file_path_b_3.clicked.connect(self.browser_tools.directory_browser)
        self.clear_b_3.clicked.connect(lambda: self.b_buttons.f_clear(3))
        self.quit_b_3.clicked.connect(self.b_buttons.f_quit)
        self.go_LIN_b.clicked.connect(self.c.f_LIN)

        self.file_path_b_4.clicked.connect(self.browser_tools.trans_browser_i)
        self.file_path_b_5.clicked.connect(self.browser_tools.trans_browser_o)
        self.quit_b_4.clicked.connect(self.b_buttons.f_quit)
        self.go_translate.clicked.connect(self.c.f_translate)

        self.file_path_b_6.clicked.connect(self.browser_tools.spe_browser)
        self.clear_SPE_b.clicked.connect(lambda: self.b_buttons.f_clear(4))
        self.quit_SPE_b.clicked.connect(self.b_buttons.f_quit)
        self.go_SPE_b.clicked.connect(self.c.f_findSPE)
        self.redraw_b_2.clicked.connect(self.b_buttons.f_spe_redraw_b)


        # Signals
        self.spinBox_pmt.valueChanged.connect(self.b_buttons.f_coeff_update)
        self.spinBox_pmt.valueChanged.connect(self.b_buttons.mirror_DBLR2RES)
        self.spinBox_pmt_2.valueChanged.connect(self.b_buttons.f_coeff_update)
        self.spinBox_pmt_2.valueChanged.connect(self.b_buttons.mirror_RES2DBLR)
        self.spinBox_pmt_3.valueChanged.connect(self.b_buttons.f_coeff_update)
        self.spinBox_pmt_3.valueChanged.connect(self.b_buttons.mirror_LIN2DBLR)
        self.spinBox_i.valueChanged.connect(self.b_buttons.mirror_DBLR2RES)
        self.spinBox_thre.valueChanged.connect(self.b_buttons.mirror_DBLR2RES)
        self.spinBox_thre_2.valueChanged.connect(self.b_buttons.mirror_RES2DBLR)
        self.spinBox_thre_3.valueChanged.connect(self.b_buttons.mirror_LIN2DBLR)

        self.blr_t.editingFinished.connect(self.b_buttons.mirror_DBLR2RES)
        self.cf_t.editingFinished.connect(self.b_buttons.mirror_DBLR2RES)
        self.thr_t.editingFinished.connect(self.b_buttons.mirror_DBLR2RES)
        self.accum_t.editingFinished.connect(self.b_buttons.mirror_DBLR2RES)
        self.spe_t_2.editingFinished.connect(self.b_buttons.mirror_RES2DBLR)
        self.spe_t.editingFinished.connect(self.b_buttons.mirror_DBLR2RES)
        self.spe_t_3.editingFinished.connect(self.b_buttons.mirror_LIN2DBLR)
        self.bins_t.editingFinished.connect(self.b_buttons.mirror_RES2DBLR)
        self.bins_t_2.editingFinished.connect(self.b_buttons.mirror_LIN2DBLR)
        self.Low_limit_t.editingFinished.connect(self.b_buttons.mirror_RES2DBLR)
        self.Low_limit_t_2.editingFinished.connect(self.b_buttons.mirror_LIN2DBLR)
        self.High_limit_t.editingFinished.connect(self.b_buttons.mirror_RES2DBLR)
        self.High_limit_t_2.editingFinished.connect(self.b_buttons.mirror_LIN2DBLR)
        self.events_t.editingFinished.connect(self.b_buttons.mirror_RES2DBLR)
        self.events_t_2.editingFinished.connect(self.b_buttons.mirror_LIN2DBLR)
        self.file_path_t_2.editingFinished.connect(self.b_buttons.mirror_RES2DBLR)
        self.events_t.editingFinished.connect(self.b_buttons.store_data)
        self.events_t_2.editingFinished.connect(self.b_buttons.store_data)
        self.prefix_t.editingFinished.connect(self.b_buttons.store_data)
        self.file_path_t_3.editingFinished.connect(self.b_buttons.store_data)
        self.conf_file_t.editingFinished.connect(self.b_buttons.store_data)
        self.file_path_t.editingFinished.connect(self.b_buttons.store_data)
        self.file_path_t_2.editingFinished.connect(self.b_buttons.store_data)

        self.spinBox_pmt_4.valueChanged.connect(self.b_buttons.store_data)
        self.spinBox_pmt_5.valueChanged.connect(self.b_buttons.store_data)
        self.file_path_t_8.editingFinished.connect(self.b_buttons.store_data)

        #SPE
        self.file_path_t_6.editingFinished.connect(self.b_buttons.store_data)
        self.spinBox_pmt_6.valueChanged.connect(self.b_buttons.store_data)
        self.spe_period.editingFinished.connect(self.b_buttons.store_data)
        self.spe_t_start.editingFinished.connect(self.b_buttons.store_data)
        self.spe_t_guess.editingFinished.connect(self.b_buttons.store_data)
        self.spinBox_spe_range.valueChanged.connect(self.b_buttons.store_data)
        self.bins_t_3.editingFinished.connect(self.b_buttons.store_data)
        self.events_t_4.editingFinished.connect(self.b_buttons.store_data)
        self.High_limit_t_3.editingFinished.connect(self.b_buttons.store_data)
        self.Low_limit_t_3.editingFinished.connect(self.b_buttons.store_data)



    def addmpl_1(self, fig):
        # Matplotlib constructor
        self.canvas1 = FigureCanvas(fig)
        self.mpl_lay.addWidget(self.canvas1)
        self.canvas1.draw()
        self.toolbar = NavigationToolbar(self.canvas1, self.frame_plot,
                                         coordinates=True)
        self.mpl_lay.addWidget(self.toolbar)
        self.d.axes['ax1'] = fig.add_subplot(111)
        self.d.axes['ax2'] = self.d.axes['ax1'].twinx()
        self.d.axes['ax1'].set_xlabel("Samples")
        self.d.axes['ax1'].set_ylabel("ADC counts")
        self.d.axes['ax1'].legend(" ")    
        

    def addmpl_2(self, fig):
        # Matplotlib constructor
        self.canvas2 = FigureCanvas(fig)
        self.mpl_lay2.addWidget(self.canvas2)
        self.canvas2.draw()
        self.toolbar = NavigationToolbar(self.canvas2, self.frame_plot_2,
                                         coordinates=True)
        self.mpl_lay2.addWidget(self.toolbar)
        self.d.axes['ax3'] = fig.add_subplot(111)

    def addmpl_3(self, fig):
        # Matplotlib constructor
        self.canvas3 = FigureCanvas(fig)
        self.mpl_lay3.addWidget(self.canvas3)
        self.canvas3.draw()
        self.toolbar = NavigationToolbar(self.canvas3, self.frame_plot_3,
                                         coordinates=True)
        self.mpl_lay3.addWidget(self.toolbar)
        self.d.axes['ax4'] = fig.add_subplot(111)
        self.d.axes['ax4'].set_xlabel("Time (us)")
        self.d.axes['ax4'].set_ylabel("Photons") 

    def addmpl_4(self, fig):
        # Matplotlib constructor
        self.canvas4 = FigureCanvas(fig)
        self.mpl_lay4.addWidget(self.canvas4)
        self.canvas4.draw()
        self.toolbar = NavigationToolbar(self.canvas4, self.frame_plot_4,
                                         coordinates=True)
        self.mpl_lay4.addWidget(self.toolbar)
        self.d.axes['ax5'] = fig.add_subplot(111)
        self.d.axes['ax5'].set_xlabel("Counts (25ns)")
        self.d.axes['ax5'].set_ylabel("Hits")


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.addmpl_1(window.d.fig1)
    window.addmpl_2(window.d.fig2)
    window.addmpl_3(window.d.fig3)
    window.addmpl_4(window.d.fig4)
    window.show()
    sys.exit(app.exec_())
