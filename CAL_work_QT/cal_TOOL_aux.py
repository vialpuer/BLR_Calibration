import sys
import os
import numpy as np
from scipy import signal as SGN
import matplotlib.pyplot as plt
from fit_library import line_fit
from cal_IO_lib import get_CALHF
from cal_IO_lib import read_CALHF
from scipy.optimize import curve_fit
from scipy.optimize import leastsq
from scipy.optimize import least_squares
from scipy import signal as sgn
from DBLR_lab import find_baseline
from DBLR_lab import energy_measure
from DBLR_lab import BLR_ACC3_i_CAL
from MP_wrappers import BLR_lambda
from MP_wrappers import BLR_batch
import csv
import pandas as pd



########### Error Functions ###########

def Efunc_cfilt(tpl, c_filt_range , f, blr_guess):

    signal_a,a,s = BLR_ACC3_i_CAL(signal_daq = f,
                            coef = blr_guess,
                            cal_range = c_filt_range,
                            thr = 5,
                            coef_clean = tpl[0])

    factor = np.mean(signal_a[c_filt_range]) #+np.std(signal_a[c_filt_range])

    return factor


def Efunc_blr(tpl, blr_range, cal_range, f, c_filt, TAU):

    signal_o,a,s = BLR_ACC3_i_CAL(signal_daq=f,
                            coef=tpl[0],
                            cal_range = cal_range,
                            thr = 5,
                            coef_clean=c_filt)

    piece = signal_o[blr_range]
    area_norm = signal_o[blr_range[0]]*TAU # Approx. of Tail Integral

    factor_1 = np.sum(np.square(piece))/area_norm
    factor_2 = 40*np.sum(np.square((piece<0)*piece))/area_norm
    factor = factor_1 + factor_2

    print (np.sum(np.square(piece)) , area_norm, np.sum(np.square((piece<0)*piece)))

    return factor



######## Calibration TOOL

def cal_TOOL(   path_file,
                channel = 0,
                event_range = range(0,5),
                c_filt_range = range(60000,120000),
                blr_guess = 0.53E-3,
                pulse_start_point = 20140,
                TAU1 = 150,
                TAU2 = 3000
                ):


    signal = read_CALHF(path_file,channel,event_range)


    # blr_s_p is the start point to start to look for the local maximum
    # After that we have a TAU2 discharge where the optimizer looks
    # For the minimum positive area
    #(20140 is the pulse start in the buffer)

    # Beginning and End of length number
    b_ch = path_file.find('/box')+len('/box')+2
    e_ch = path_file[b_ch:].find('u')+b_ch
    pulse_length = int(path_file[b_ch:e_ch])

    blr_start_point = pulse_start_point + pulse_length*40 + 8

    print (" Start to look for local maximum at %d " % blr_start_point)


    signal_off = np.sum(signal[:,:],axis=1) / float(len(event_range))

    EFunc_Lambda = lambda tpl: Efunc_cfilt( tpl,
                                            c_filt_range,
                                            signal_off,
                                            blr_guess,
                                            )

    coeff = least_squares(  EFunc_Lambda,
                            x0=0.25E-6,
                            bounds=(0.001E-6,4.0e-06),
                            ftol=1e-10,
                            xtol=1e-10,
                            gtol=1e-15)


    sig_1 = BLR_ACC3_i_CAL( signal_daq = signal_off,
                            coef = blr_guess,
                            cal_range = c_filt_range,
                            thr = 5,
                            coef_clean = coeff['x'])[0]

    c_filt = coeff['x']

    ########################################################################

    blr_start = np.argmax(sig_1[blr_start_point:blr_start_point+400])+blr_start_point

    print (" Local maximum found at %d " % blr_start)

    blr_range = range(blr_start,blr_start+TAU2*3)

    EFunc_BLR_Lambda = lambda tpl: Efunc_blr(   tpl,
                                                blr_range,
                                                c_filt_range,
                                                signal_off,
                                                c_filt,
                                                TAU=TAU1
                                                )

    blr = least_squares(EFunc_BLR_Lambda, x0=blr_guess, bounds=(0.2E-3,0.9E-3))

    blr_coeff = blr['x']


    sig_2 = BLR_ACC3_i_CAL( signal_daq = signal_off,
                            coef = blr_coeff,
                            cal_range = c_filt_range,
                            thr = 5,
                            acum_FLOOR = -1E10,
                            coef_clean = c_filt )[0]

    MAU=np.ones(200)/200.0
    out_MAU = sgn.lfilter(MAU,1,sig_2)

    #fig, ax1 = plt.subplots();
    #ax1.plot(range(0,len(sig_1)), sig_1, 'g', linewidth=1)
    #ax1.plot(range(0,len(sig_2)), sig_2, 'r', linewidth=1)
    #ax1.plot(range(0,len(out_MAU)), out_MAU, 'b', linewidth=1)

    #axes = plt.gca();
    #plt.show()

    coeffs={'c_filt':c_filt,'blr':blr_coeff}

    return coeffs, sig_1, sig_2, out_MAU


def main():
    site = "/mnt/WINDOWS_ntfs/"
    #site = "F:/"
    path_file = site + "DATOS_DAC/LINEALIDAD_7C_2/"

    PULSES = [150]
    CHANNELS = [4]
    BOX = 1

    # Some general fit parameters
    tau_1 = 150
    tau_2 = 3000
    sp = 10140
    blr_guess = 0.535E-3


    df=pd.DataFrame(data=np.zeros((4*24,4)),columns=['BOX','CH','BLR','C_FILT'])

    counter = 0

    for i in CHANNELS:
        for j in PULSES:

            df['BOX'][counter] = BOX
            df['CH'][counter]  = i

            file_name = path_file + "box" + str(BOX) + "_" + str(j) + "u.h5"

            coeffs = cal_TOOL(  file_name,
                                channel=i,
                                event_range = range(0,250),
                                c_filt_range = range(60000,120000),
                                blr_guess = blr_guess,
                                pulse_start_point = sp,
                                TAU1 = tau_1,
                                TAU2 = tau_2)

            df['BLR'][counter] = coeffs['blr']
            df['C_FILT'][counter] = coeffs['c_filt']

            counter = counter + 1

            print(" BLR    = %0.5e" % coeffs['blr'])
            print(" C_FILT = %0.5e" % coeffs['c_filt'])

    print (df)

    df.to_csv('FECal_report.csv',float_format='%.5e')


    #########################################################################
    #### Configuration File Writing
    #########################################################################

    df2=pd.DataFrame(data=np.zeros((24,2),dtype=float),columns=['BLR','CLN'])
    pulses = 3
    channels = 24
    data_file = open('FECal_report.csv')
    data_reader = csv.reader(data_file)

    FECal_DATA = np.array(list(data_reader))
    FECal_DATA = FECal_DATA[1:,:].astype(np.float)

    print (FECal_DATA)

    FE_Param = np.zeros((24,2))

    # Compute Definitive Coefficients

    for i in range(0,24):

        ch  = FECal_DATA[i*pulses,2]
        box = FECal_DATA[i*pulses,1]
        FE_Param[ch,0] = FECal_DATA[i*pulses,3]*0.3+FECal_DATA[i*pulses+1,3]*0.3+FECal_DATA[i*pulses+2,3]*0.4
        FE_Param[ch,1] = FECal_DATA[i*pulses,4]*0.3+FECal_DATA[i*pulses+1,4]*0.3+FECal_DATA[i*pulses+2,4]*0.4

        # Only use 50u, 100u and 200u. The longer the more reliable

    df2.loc[:,:]=FE_Param

    print (df2)
    df2.to_csv('COEFF_box'+str(box)+'.conf',float_format='%.4e')



if __name__ == "__main__":
	main()
