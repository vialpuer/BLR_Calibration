import sys
import numpy as np
import csv
import os as os
import pandas as pd
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtWidgets import QMessageBox
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

# My functions
import cal_TOOL_aux as ct
# from MP_wrappers import BLR_lambda
# from MP_wrappers import BLR_batch
# from cal_IO_lib import read_CALHF
# from cal_IO_lib import read_DATE_hdf5
# from cal_IO_lib import DATE_to_CALHF_MP
# from DBLR_lab import find_baseline
# from fit_library import gauss
# import fit_library as fit


qtCreatorFile = "CAL_WORK.ui" # Enter file here.

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class LAB_data():
    fig1 = Figure()
    fig2 = Figure()
    fig3 = Figure()
    fig4 = Figure()

    axes={'ax1':0,'ax2':0}
    # Axes 1 & 2 for figure 1. Axes 3 for figure 2
    #baseline_txt = fig1.text(0.6,0.8, ('BASELINE = '))

    site = "/mnt/WINDOWS_ntfs/"
    #site = "F:/"
    def_path = site + "DATOS_DAC/CALIBRATION_FEB_2017_OB/"

    files = {'report_file':"FECal_report.csv",
          'conf_name':"COEFF.conf",
          'dir':site + "DATOS_DAC/CALIBRATION_FEB_2017_OB/"}

    fit_param = {   'start':20140,
                    'tau_1':150,
                    'tau_2':3000,
                    'blr_guess':0.25E-3}

    PMT_data = {'PMT_n':0, 'event_n':0, 'blr':0.53E-3, 'cf':0.3E-6,
                  'thr':5, 'spe':20.0, 'accum_floor':3500, 'i':1, 'thre':5}

    PULSES = "200"
    CHANNELS = "0,2,4"
    BOX = "0"

    mail_box = False

    df_report=pd.DataFrame(columns=['BOX','CH','BLR','C_FILT'])



class CORE():
    def __init__(self,upper_class):
        self.uc = upper_class




class Aux_Buttons():
    def __init__(self,upper_class):
        self.uc = upper_class

    def f_quit(self):
        print('EXIT')
        QtCore.QCoreApplication.instance().quit()

    def f_mail_box(self):
        self.uc.d.mail_box = False

    def f_report(self,option):


        self.uc.d.df_report.to_csv(self.uc.d.files['report_file'],float_format='%.5e')

        #########################################################################
        #### Configuration File Writing
        #########################################################################


        df2=pd.DataFrame(data=np.zeros((8,2),dtype=float),columns=['BLR','C_FILT'])

        PULSES_aux = np.fromstring(self.uc.d.PULSES, dtype=int, sep=',')
        CHANNELS_aux = np.fromstring(self.uc.d.CHANNELS, dtype=int, sep=',')

        pulses = np.shape(PULSES_aux)[0]
        chs = np.shape(CHANNELS_aux)[0]
        
        print (pulses)

        FE_Param = np.zeros((8,2))

        # Compute Definitive Coefficients

        print (self.uc.d.df_report)

        print (self.uc.d.df_report['CH'][0])

        for i in range(0,chs):

            ch  = self.int_v(self.uc.d.df_report['CH'][self.int_v(i*pulses)])
            print (ch)

            aux = {'BLR':0.0, 'C_FILT':0.0}

            for j in range(0,pulses):
                aux['BLR'] = self.uc.d.df_report['BLR'][self.int_v(i*pulses+j)]/pulses + aux['BLR']
                aux['C_FILT'] = self.uc.d.df_report['C_FILT'][self.int_v(i*pulses+j)]/pulses + aux['C_FILT']

            df2['BLR'][ch] = aux['BLR']
            df2['C_FILT'][ch] = aux['C_FILT']

            # Only use 50u, 100u and 200u. The longer the more reliable

        #Write COnfiguration File for the BOX
        df2.to_csv('COEFF_box'+str(self.uc.d.BOX)+'.conf',float_format='%.4e')

        #Erase memory
        self.uc.d.df_report=pd.DataFrame(columns=['BOX','CH','BLR','C_FILT'])



    def f_calibrate(self,option):

        CHANNELS = np.fromstring(self.uc.d.CHANNELS, dtype=int, sep=',')
        PULSES = np.fromstring(self.uc.d.PULSES, dtype=int, sep=',')
        BOX = self.int_v(self.uc.d.BOX)
        path_file = self.uc.d.files['dir']

        #self.uc.d.df_report = pd.DataFrame(columns=['BOX','CH','BLR','C_FILT'])

        lock_all = False

        print (CHANNELS)
        print (PULSES)

        self.uc.d.axes['ax1'].cla()
        self.uc.d.axes['ax2'].cla()
        self.uc.canvas1.draw()

        for i in CHANNELS:
            for j in PULSES:

                file_name = path_file + "box" + str(BOX) + "_" + str(j) + "u.h5"

                coeffs, sig_1, sig_2, out_MAU = ct.cal_TOOL(  file_name,
                                    channel=i,
                                    event_range = range(0,250),
                                    c_filt_range = range(60000,120000),
                                    blr_guess = self.uc.d.fit_param['blr_guess'],
                                    pulse_start_point = self.uc.d.fit_param['start'],
                                    TAU1 = self.uc.d.fit_param['tau_1'],
                                    TAU2 = self.uc.d.fit_param['tau_2'])

                #self.uc.d.axes['ax1'].cla()
                #self.uc.d.axes['ax1'].cla()
                #self.uc.d.axes['ax2'].cla()
                #self.uc.d.axes['ax1'].plot(sig_1,'r--', linewidth=1, label=)
                self.uc.d.axes['ax1'].plot(sig_2, linewidth=1, label='Result')
                self.uc.d.axes['ax2'].plot(out_MAU, linewidth=1, label='MAU filtered')
                self.uc.canvas1.draw()

                self.uc.textBrowser.append("-----------")
                self.uc.textBrowser.append("CHANNEL = " + str(i))
                self.uc.textBrowser.append("PULSE = " + str(j))
                self.uc.textBrowser.append("BLR = " + str(coeffs['blr']))
                self.uc.textBrowser.append("C_FILT = " + str(coeffs['c_filt']))

                # if lock_all == False:
                #     msg = "Store Coeff Data of this Pulse?"
                #     reply = QMessageBox.question(self.uc, 'Message',
                #             msg, QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel | QMessageBox.Abort, QMessageBox.Yes)
                #
                #     if reply == QMessageBox.Yes:
                #
                #         df_aux = pd.DataFrame(data=[[BOX,i,coeffs['blr'],coeffs['c_filt']]],
                #                           columns=['BOX','CH','BLR','C_FILT'])
                #         self.uc.d.df_report=self.uc.d.df_report.append(df_aux,
                #                                                    ignore_index=True)
                #     if reply == QMessageBox.No:
                #         #Store a wrong value to show a bad calibration
                #         df_aux = pd.DataFrame(data=[[BOX,i,100,100]],
                #                               columns=['BOX','CH','BLR','C_FILT'])
                #         self.uc.d.df_report=self.uc.d.df_report.append(df_aux,
                #                                                    ignore_index=True)
                #     if reply == QMessageBox.Cancel:
                #         lock_all = True
                #         df_aux = pd.DataFrame(data=[[BOX,i,coeffs['blr'],coeffs['c_filt']]],
                #                           columns=['BOX','CH','BLR','C_FILT'])
                #         self.uc.d.df_report=self.uc.d.df_report.append(df_aux,
                #                                                    ignore_index=True)
                #
                #
                #     if reply == QMessageBox.Abort:
                #         print('CANCEL')
                #         break
                #
                # else:
                #     df_aux = pd.DataFrame(data=[[BOX,i,coeffs['blr'],coeffs['c_filt']]],
                #                       columns=['BOX','CH','BLR','C_FILT'])
                #     self.uc.d.df_report=self.uc.d.df_report.append(df_aux,
                #                                                ignore_index=True)



                df_aux = pd.DataFrame(data=[[BOX,i,coeffs['blr'],coeffs['c_filt']]],
                                  columns=['BOX','CH','BLR','C_FILT'])
                self.uc.d.df_report=self.uc.d.df_report.append(df_aux,
                                                           ignore_index=True)



            else:
                continue

            break




    # Controlled casting to avoid data intro errors
    def float_v(self,number):
        try:
            return float(number)
        except ValueError:
            return 0.0

    def int_v(self,number):
        try:
            return int(number)
        except ValueError:
            return 0

    def store_data(self):
        self.uc.d.files['dir'] = self.uc.lineEdit_cal_file_path.text()
        self.uc.d.PULSES = self.uc.lineEdit_pulse_lengths.text()
        self.uc.d.CHANNELS = self.uc.lineEdit_channels.text()
        self.uc.d.BOX = self.int_v(self.uc.lineEdit_box.text())
        self.uc.d.fit_param['start'] = self.int_v(self.uc.lineEdit_pulse_start.text())
        self.uc.d.fit_param['tau_1'] = self.int_v(self.uc.lineEdit_tau1.text())
        self.uc.d.fit_param['tau_2'] = self.int_v(self.uc.lineEdit_tau2.text())
        self.uc.d.fit_param['blr_guess'] = self.float_v(self.uc.lineEdit_blr_guess.text())
        self.uc.d.files['report_file'] = self.uc.lineEdit_cal_report.text()
        self.uc.d.files['conf_name'] = self.uc.lineEdit_conf_file.text()


class Browsers():
    def __init__(self,upper_class):
        self.uc = upper_class

    def path_browser(self):
        file_aux = QtWidgets.QFileDialog.getExistingDirectory(self.uc,
                                        'Open Directory',
                                        self.uc.d.files['dir'])

        self.uc.d.files['dir'] = file_aux+'/'
        #Trick for Qstring converting to standard string
        self.uc.lineEdit_cal_file_path.setText(self.uc.d.files['dir'])



class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        #Data class
        self.d = LAB_data()
        self.c = CORE(self)
        self.browser_tools = Browsers(self)
        self.b_buttons = Aux_Buttons(self)
        # Passes all the needed information to the constructor of the aux class

        #Defaults
        self.lineEdit_cal_file_path.setText(self.d.files['dir'])
        self.lineEdit_pulse_lengths.setText(str(self.d.PULSES))
        self.lineEdit_channels.setText(str(self.d.CHANNELS))
        self.lineEdit_box.setText(str(self.d.BOX))
        self.lineEdit_pulse_start.setText(str(self.d.fit_param['start']))
        self.lineEdit_tau1.setText(str(self.d.fit_param['tau_1']))
        self.lineEdit_tau2.setText(str(self.d.fit_param['tau_2']))
        self.lineEdit_blr_guess.setText(str(self.d.fit_param['blr_guess']))
        self.lineEdit_cal_report.setText(str(self.d.files['report_file']))
        self.lineEdit_conf_file.setText(str(self.d.files['conf_name']))

        #Button Calls
        self.pushButton_quit.clicked.connect(self.b_buttons.f_quit)
        self.toolButton_browser.clicked.connect(self.browser_tools.path_browser)
        self.pushButton_calibrate.clicked.connect(self.b_buttons.f_calibrate)
        self.pushButton_report.clicked.connect(self.b_buttons.f_report)
        self.pushButton_next.clicked.connect(self.b_buttons.f_mail_box)
        #self.clear_b_2.clicked.connect(lambda: self.b_buttons.f_clear(2))


        # Signals
        self.lineEdit_cal_file_path.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_pulse_lengths.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_channels.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_box.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_pulse_start.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_tau1.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_tau2.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_blr_guess.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_cal_report.editingFinished.connect(self.b_buttons.store_data)
        self.lineEdit_conf_file.editingFinished.connect(self.b_buttons.store_data)




    def addmpl(self, fig):
        # Matplotlib constructor
        self.canvas1 = FigureCanvas(fig)
        self.mpl_lay.addWidget(self.canvas1)
        self.canvas1.draw()
        self.toolbar = NavigationToolbar(self.canvas1, self.frame_2,
                                         coordinates=True)
        self.mpl_lay.addWidget(self.toolbar)
        self.d.axes['ax1'] = fig.add_subplot(111)
        self.d.axes['ax2'] = self.d.axes['ax1'].twinx()
        #self.d.axes['ax3'] = self.d.axes['ax1'].twinx()




if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.addmpl(window.d.fig1)
    window.show()
    sys.exit(app.exec_())
